/* É o tipo de coisa que eu faria usando uma aproximação mais funciona,
com filter, find, etc. Mas para manter ao escopo da aula, vou fazer com filtro mesmo. */

/* Quanto a linguagem do código (português ou inglês), eu tenho preferência em fazer em inglês,
pois segue o padrão da linguagem de programação e dos padrões internacionais de nomeação, que ficam
mais simples e curtos em inglês.

Porém, como "turma" em inglês é "class" e class é uma palavra protegida, eu vou usar "team" para me 
referir a turma.*/

// para teste
const students = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7,
    },
    {
        "nome": "Matheus",
        "turma": "A",
        "nota1": 6,
        "nota2": 8,
    },
    {
        "nome": "Tiago",
        "turma": "B",
        "nota1": 10,
        "nota2": 9,
    },
    {
        "nome": "João",
        "turma": "B",
        "nota1": 10,
        "nota2": 8,
    },
    {
        "nome": "Bianca",
        "turma": "B",
        "nota1": 8,
        "nota2": 9,
    },
    {
        "nome": "Beatriz",
        "turma": "B",
        "nota1": 7,
        "nota2": 10,
    },
    {
        "nome": "Gyselle",
        "turma": "C",
        "nota1": 10,
        "nota2": 10,
    },
    {
        "nome": "Marcos",
        "turma": "A",
        "nota1": 4,
        "nota2": 7,
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    }, 
    {
        "nome": "Jimmy",
        "turma": "A",
        "nota1": 5,
        "nota2": 4
    }, 
];

/** Cria uma objeto de alunos usando a turma como chave. */
function groupStudentsByClass(studentList) {
    let teams = {};
    for (const student of studentList) {
        let team = student['turma'];
        
        if (teams[team] === undefined) {
            teams[team] = [];
        }

        teams[team].push(student);
    }
    return teams;
}

/** Recebe uma lista de alunos e retorna o aluno que possui a maior média de notas. */
function getSmarterStudant(studantList) {
    let smarter;
    let biggerAvg = 0;

    for (const student of studantList) {
        let avg = getStudentAverage(student);
        if (avg > biggerAvg) {
            smarter = student;
            biggerAvg = avg;
        }
    }
    return smarter;
}

function getStudentAverage(student) {
    return (student['nota1'] + student['nota2']) / 2;
}

function displayStudent(student) {
    console.log(`Nome: ${student['nome']},`, 
                `Turma: ${student['turma']},`,
                `Média: ${getStudentAverage(student)}`);
}

function main() {
    const teamsOfStudents = groupStudentsByClass(students)
    let smarterByClass = {}

    for (const key in teamsOfStudents) {
        smarterByClass[key] = getSmarterStudant(teamsOfStudents[key])
    }

    for (const key in smarterByClass) {
        displayStudent(smarterByClass[key]);
    }
}


main()
