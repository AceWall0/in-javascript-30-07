function removeDuplicate(list) {
    let newList = [];
    for (let elem of list) {
        if (newList.indexOf(elem) === -1) {
            newList.push(elem);
        }
    }
    return newList;
}


console.group("Remove duplicatas")
const arrayWithDuplicates = [1, 2, 2, 3, 7, 4, 3, 2, 1];
console.log("Input", arrayWithDuplicates)
console.log("Output", removeDuplicate(arrayWithDuplicates))
console.groupEnd("Remove duplicatas")
console.log()


function compareArrays(array1, array2) {
    if (array1.length != array2.length) {
        return false;
    }

    let sorted1 = array1.sort();
    let sorted2 = array2.sort();

    for (let i in sorted1) {
        if (sorted1[i] != sorted2[i]) {
            return false;
        }
    }

    return true;
}

console.group("Compara arrays")
const test1a = [1, 2 ,3];
const test2a = [3, 2 ,1];
console.log("Inputs", test1a, test2a)
console.log("Output", compareArrays(test1a, test2a));


const test1b = [4, 5, 6];
const test2b = [7, 5, 3];
console.log("Inputs", test2a, test2b)
console.log("Output", compareArrays(test1b, test2b));

console.groupEnd("Compara arrays")
console.log()



function addMatrixes(arr1, arr2) {
    // Vou supor que todos os arrays passados sejam quadrados, para simplificação.
    const n = arr1.length;
    let newArr = [];

    for (let i = 0; i < n; i++) {
        let line = []
        for (let j = 0; j < n; j++) {
            line.push(arr1[i][j] + arr2[i][j])
        }
        newArr.push(line)
    }

    return newArr;
}


console.group("Adiciona matrizes")
const matrix1 = [
    [1, 2],
    [3, 4]
]
const matrix2 = [
    [5, 6],
    [7, 8]
]

console.log("Inputs:", matrix1, matrix2)
console.log("Output", addMatrixes(matrix1, matrix2))

console.groupEnd("Adiciona matrizes")